package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @title: AppBootstrap
 * @Author hqgordon
 * @Date: 2021/8/3 5:08 下午
 * @Description: 主启动类
 * @Version 1.0
 */
@SpringBootApplication
public class AppBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(AppBootstrap.class,args);
    }
}