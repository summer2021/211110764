**shenyu网关接入sureness的两个数据源版本**

shenyu网关需要配置网关配套的shenyu-admin是shenyu的配置中心，在其中可以开启关闭插件，配置插件等。
        application.yml中可配置你的shenyu配置中心地址


搭建步骤：idea 导入pom文件新建工程，启动主启动类即可。
        
        
测试步骤：修改application.yml中的数据库用户名和密码，启动主启动类，
        sql版本第一次运行会自动创建数据库。
        可通过postman等工具来访问测试，访问带上yml或者sql版本里的username和password。

插件版本:


1.将模块添加到incubator-shenyu中 使用shenyu-bootstrap网关

2.先添加shenyu-plugin-surness_yml到shenyu-plugin模块下

3.再添加shenyu-spring-boot-stater-plugin-sureness到shenyu-spring-boot-stater-plugin下

4.然后在bootstrap里添加权限yml文件，在bootstrap的pom文件里引入shenyu-spring-boot-stater-plugin-sureness

启动shenyu网关admin，进入配置前端， 添加插件sureness 添加规则

测试成功




